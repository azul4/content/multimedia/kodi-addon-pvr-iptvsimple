# kodi-addon-pvr-iptvsimple

IPTV Simple PVR client addon for Kodi 20, code name **Nexus**

https://github.com/kodi-pvr/pvr.iptvsimple

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/kodi-addon-pvr-iptvsimple.git
```

